SHELL = /bin/bash
pre-setup:;
	@echo "Creating environment file from template"
	@rm -f .env && envsubst < config/.env.sample > .env
setup:;
	@echo "Generating secret files from templates using environment file + variables"
	@source .env && rm -f ./config/application/secret.properties && envsubst < config/application/secret.properties.sample | tr -d '\r' > config/application/secret.properties
dev-start:;
	mvn compile -e quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties
clean:;
	mvn clean
compile-java: generate-spec;
	mvn compile package
compile-java-quick: generate-spec;
	mvn compile package -Dmaven.test.skip=true
compile: clean compile-java;
compile-quick: clean compile-java-quick;
install-yarn:;
	yarn install --frozen-lockfile --audit
generate-spec: install-yarn validate-spec;
	yarn run generate-json-schema
validate-spec: install-yarn;
compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d
start-spec: validate-spec;
	yarn run start
