/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.resources;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.mailing.api.AccountsAPI;
import org.eclipsefoundation.mailing.api.AccountsAPI.EclipseUser;
import org.eclipsefoundation.mailing.dto.MailingListSubscriptions;
import org.eclipsefoundation.mailing.dto.MailingLists;
import org.eclipsefoundation.mailing.namespace.MailingListUrlParamterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.client.exception.ResteasyWebApplicationException;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/mailing-lists")
@Produces(MediaType.APPLICATION_JSON)
public class MailingListsResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailingListsResource.class);

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;
    @Inject
    CachingService cache;
    @Inject
    RequestWrapper wrap;

    @Inject
    @RestClient
    AccountsAPI api;

    @GET
    public Response all(@QueryParam("username") String uid) {
        MultivaluedMap<String, String> params = getDefaultParams();
        // check for user to retrieve
        if (uid != null) {
            try {
                EclipseUser user = api.getUsers(uid);
                if (user != null && user.getMail() != null) {
                    params.add(MailingListUrlParamterNames.USER_EMAIL.getName(), user.getMail());
                }
            } catch (ResteasyWebApplicationException e) {
                LOGGER.warn("Could not find valid user with username '{}', returning empty results", uid);
                return Response.status(404).build();
            }
        }
        return Response.ok(dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params))).build();
    }

    @GET
    @Path("{listName}")
    public Response mailingList(@PathParam("listName") String listName) {
        MultivaluedMap<String, String> params = getDefaultParams();
        params.add(MailingListUrlParamterNames.LIST_NAME.getName(), listName);
        List<MailingLists> mailingLists = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));
        if (mailingLists.isEmpty()) {
            return Response.status(404).build();
        }
        // add count to singular mailing list retrieval
        MailingLists ml = mailingLists.get(0);
        ml.setCount(dao.count(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class), params)));
        return Response.ok(ml).build();
    }

    @GET
    @Path("available")
    public Response available() {
        List<MailingLists> mailingLists = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), getAvailableListParams()));
        return Response.ok(mailingLists).build();
    }

    @GET
    @Path("projects")
    public Response getByProjects(@PathParam("projectId") String projectId) {
        List<MailingLists> mailingLists = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), getDefaultParams()));
        if (mailingLists.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }

        return Response.ok(mailingLists.stream().collect(Collectors.groupingBy(MailingLists::getProjectId))).build();
    }

    @GET
    @Path("projects/{projectId}")
    public Response getForProjectByID(@PathParam("projectId") String projectId) {
        MultivaluedMap<String, String> params = getDefaultParams();
        params.add(MailingListUrlParamterNames.PROJECT_ID.getName(), projectId);
        List<MailingLists> mailingLists = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));
        if (mailingLists.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(mailingLists).build();
    }

    @GET
    @Path("projects/available")
    public Response getByProjectsAvailable(@PathParam("projectId") String projectId) {
        List<MailingLists> mailingLists = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), getAvailableListParams()));
        if (mailingLists.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }

        return Response.ok(mailingLists.stream().collect(Collectors.groupingBy(MailingLists::getProjectId))).build();
    }

    /**
     * Gets default params for available mailing lists
     * 
     * @return
     */
    private MultivaluedMap<String, String> getDefaultParams() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MailingListUrlParamterNames.IS_PRIVATE.getName(), Boolean.FALSE.toString());
        return params;
    }

    private MultivaluedMap<String, String> getAvailableListParams() {
        MultivaluedMap<String, String> params = getDefaultParams();
        params.add(MailingListUrlParamterNames.IS_DELETED.getName(), Boolean.FALSE.toString());
        params.add(MailingListUrlParamterNames.IS_DISABLED.getName(), Boolean.FALSE.toString());
        params.add(MailingListUrlParamterNames.IS_SUBSCRIBABLE.getName(), Boolean.TRUE.toString());
        return params;
    }
}