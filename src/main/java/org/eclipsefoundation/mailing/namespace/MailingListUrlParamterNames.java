/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public final class MailingListUrlParamterNames implements UrlParameterNamespace {
    public static final UrlParameter LIST_NAME = new UrlParameter("list_name");
    public static final UrlParameter PROJECT_ID = new UrlParameter("project_id");
    public static final UrlParameter IS_PRIVATE = new UrlParameter("is_private");
    public static final UrlParameter IS_SUBSCRIBABLE = new UrlParameter("is_subscribable");
    public static final UrlParameter IS_DISABLED = new UrlParameter("is_disabled");
    public static final UrlParameter IS_DELETED = new UrlParameter("is_deleted");
    public static final UrlParameter USERNAME = new UrlParameter("username");
    public static final UrlParameter USER_EMAIL = new UrlParameter("user_email");

    private static final List<UrlParameter> params = Collections.unmodifiableList(Arrays.asList(LIST_NAME, PROJECT_ID,
            IS_PRIVATE, IS_SUBSCRIBABLE, IS_DISABLED, IS_DELETED, USERNAME, USER_EMAIL));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }
}
