/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.dto;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.mailing.namespace.MailingListUrlParamterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@Table
@Entity
public class MailingLists extends BareNode {
    public static final DtoTable TABLE = new DtoTable(MailingLists.class, "ml");

    @Id
    @Nonnull
    private String listName;
    private String listDescription;
    @Nonnull
    private String projectId;
    private String listShortDescription;
    @Nonnull
    private boolean createArchives;
    @Nonnull
    private boolean isForNewsArchives;
    @Nonnull
    private boolean isDisabled;
    @Nonnull
    private boolean isDeleted;
    @Nonnull
    private boolean isPrivate;
    @Nonnull
    private boolean isSubscribable;
    private Date createDate;
    private String createdBy;
    private String provisionStatus;
    @JsonInclude(value = Include.NON_NULL)
    @Transient
    private Long count;
    @Transient
    private String url;
    @Transient
    private String email;

    @PostLoad
    public void loadAdditionalProperties() {
        this.url = "https://accounts.eclipse.org/mailing-list/" + listName;
        this.email = listName + "@eclipse.org";
    }

    @JsonIgnore
    @Override
    public Object getId() {
        return getListName();
    }

    /**
     * @return the listName
     */
    public String getListName() {
        return listName;
    }

    /**
     * @param listName the listName to set
     */
    public void setListName(String listName) {
        this.listName = listName;
    }

    /**
     * @return the listDescription
     */
    public String getListDescription() {
        return listDescription;
    }

    /**
     * @param listDescription the listDescription to set
     */
    public void setListDescription(String listDescription) {
        this.listDescription = listDescription;
    }

    /**
     * @return the projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * @return the listShortDescription
     */
    public String getListShortDescription() {
        return listShortDescription;
    }

    /**
     * @param listShortDescription the listShortDescription to set
     */
    public void setListShortDescription(String listShortDescription) {
        this.listShortDescription = listShortDescription;
    }

    /**
     * @return the createArchives
     */
    public boolean isCreateArchives() {
        return createArchives;
    }

    /**
     * @param createArchives the createArchives to set
     */
    public void setCreateArchives(boolean createArchives) {
        this.createArchives = createArchives;
    }

    /**
     * @return the isForNewsArchives
     */
    @JsonProperty(value = "is_for_news_archives")
    public boolean isForNewsArchives() {
        return isForNewsArchives;
    }

    /**
     * @param isForNewsArchives the isForNewsArchives to set
     */
    public void setForNewsArchives(boolean isForNewsArchives) {
        this.isForNewsArchives = isForNewsArchives;
    }

    /**
     * @return the isDisabled
     */
    @JsonProperty(value = "is_disabled")
    public boolean isDisabled() {
        return isDisabled;
    }

    /**
     * @param isDisabled the isDisabled to set
     */
    public void setDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    /**
     * @return the isDeleted
     */
    @JsonProperty(value = "is_deleted")
    public boolean isDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted the isDeleted to set
     */
    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return the isPrivate
     */
    @JsonProperty(value = "is_private")
    public boolean isPrivate() {
        return isPrivate;
    }

    /**
     * @param isPrivate the isPrivate to set
     */
    public void setPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    /**
     * @return the isSubscribable
     */
    @JsonProperty(value = "is_subscribable")
    public boolean isSubscribable() {
        return isSubscribable;
    }

    /**
     * @param isSubscribable the isSubscribable to set
     */
    public void setSubscribable(boolean isSubscribable) {
        this.isSubscribable = isSubscribable;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the provisionStatus
     */
    public String getProvisionStatus() {
        return provisionStatus;
    }

    /**
     * @param provisionStatus the provisionStatus to set
     */
    public void setProvisionStatus(String provisionStatus) {
        this.provisionStatus = provisionStatus;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    @Singleton
    public static class MailingListFilter implements DtoFilter<MailingLists> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(MailingListUrlParamterNames.LIST_NAME.getName());
                if (id != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".listName = ?",
                            new Object[] { id }));
                }
                // project ID check
                String projectId = params.getFirst(MailingListUrlParamterNames.PROJECT_ID.getName());
                if (projectId != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".projectId = ?",
                            new Object[] { projectId }));
                }
                // subscribable check
                String isSubscribable = params.getFirst(MailingListUrlParamterNames.IS_SUBSCRIBABLE.getName());
                if (isSubscribable != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".isSubscribable = ?",
                            new Object[] { Boolean.parseBoolean(isSubscribable) }));
                }
                // is private check
                String isPrivate = params.getFirst(MailingListUrlParamterNames.IS_PRIVATE.getName());
                if (isPrivate != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".isPrivate = ?",
                            new Object[] { Boolean.parseBoolean(isPrivate) }));
                }
                // is deleted check
                String isDeleted = params.getFirst(MailingListUrlParamterNames.IS_DELETED.getName());
                if (isDeleted != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".isDeleted = ?",
                            new Object[] { Boolean.parseBoolean(isDeleted) }));
                }
                // is disabled check
                String isDisabled = params.getFirst(MailingListUrlParamterNames.IS_DISABLED.getName());
                if (isDisabled != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".isDisabled = ?",
                            new Object[] { Boolean.parseBoolean(isDisabled) }));
                }
                // subscriber check
                String userEmail = params.getFirst(MailingListUrlParamterNames.USER_EMAIL.getName());
                if (userEmail != null) {
                    stmt.addJoin(new ParameterizedSQLStatement.Join(TABLE, MailingListSubscriptions.TABLE, "listName",
                            "listName"));
                    stmt.addClause(new ParameterizedSQLStatement.Clause(
                            MailingListSubscriptions.TABLE.getAlias() + ".email = ?", new Object[] { userEmail }));
                }
            }
            return stmt;
        }

        @Override
        public Class<MailingLists> getType() {
            return MailingLists.class;
        }

    }
}
