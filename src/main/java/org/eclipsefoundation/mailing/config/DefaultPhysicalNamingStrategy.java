/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.config;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import com.google.common.base.CaseFormat;

public class DefaultPhysicalNamingStrategy implements PhysicalNamingStrategy {

	@Override
	public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
	}

	@Override
	public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
	}

	@Override
	public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
	}

	@Override
	public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
	}

	@Override
	public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
	}

	private Identifier convert(CaseFormat originFormat, CaseFormat outputFormat, Identifier source) {
		if (source == null || source.getText() == null) {
			return source;
		}
		return new Identifier(originFormat.to(outputFormat, source.getText()), source.isQuoted());
	}
}
