/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.resources;

import org.eclipsefoundation.mailing.test.namespace.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates.EndpointTestCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
class MailingListsResourceTests {
    public static final String MAILING_LISTS_BASE_URL = "/mailing-lists";
    public static final String AVAILABLE_MAILING_LISTS_URL = MAILING_LISTS_BASE_URL + "/available";
    public static final String MAILING_LISTS_BY_NAME_URL = MAILING_LISTS_BASE_URL + "/{listName}";

    public static final String PROJECTS_MAILING_LISTS_URL = MAILING_LISTS_BASE_URL + "/projects";
    public static final String AVAILABLE_PROJECTS_MAILING_LISTS_URL = PROJECTS_MAILING_LISTS_URL + "/available";
    public static final String PROJECT_MAILING_LISTS_URL = PROJECTS_MAILING_LISTS_URL + "/{projectID}";

    /*
     * GET ALL
     */
    public static final EndpointTestCase GET_ALL_SUCCESS = TestCaseHelper.buildSuccessCase(MAILING_LISTS_BASE_URL,
            new String[] {}, SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_ALL_INVALID_FORMAT = TestCaseHelper.buildInvalidFormatCase(
            MAILING_LISTS_BASE_URL, new String[] {}, ContentType.TEXT);

    /*
     * GET AVAILABLE
     */
    public static final EndpointTestCase GET_AVAILABLE_SUCCESS = TestCaseHelper.buildSuccessCase(
            AVAILABLE_MAILING_LISTS_URL, new String[] {}, SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_AVAILABLE_INVALID_FORMAT = TestCaseHelper.buildInvalidFormatCase(
            AVAILABLE_MAILING_LISTS_URL, new String[] {}, ContentType.XML);

    /*
     * BY NAME
     */
    public static final EndpointTestCase GET_MAILING_LISTS_BY_NAME_SUCCESS = TestCaseHelper.buildSuccessCase(
            MAILING_LISTS_BY_NAME_URL, new String[] { "eclipse-dev" }, SchemaNamespaceHelper.MAILING_LIST_SCHEMA_PATH);

    public static final EndpointTestCase GET_MAILING_LISTS_BY_NAME_INVALID_FORMAT = TestCaseHelper
            .buildInvalidFormatCase(MAILING_LISTS_BY_NAME_URL, new String[] { "eclipse-dev" }, ContentType.XML);

    public static final EndpointTestCase GET_MAILING_LISTS_BY_NAME_NOT_FOUND = TestCaseHelper.buildNotFoundCase(
            MAILING_LISTS_BY_NAME_URL, new String[] { "elipse-dem" });

    /*
     * GET PROJECTS MAILING LISTS
     */
    public static final EndpointTestCase GET_PROJECTS_MAILING_LISTS_SUCCESS = TestCaseHelper.buildSuccessCase(
            PROJECTS_MAILING_LISTS_URL, new String[] {}, SchemaNamespaceHelper.MAILING_LIST_MAPPING_SCHEMA_PATH);

    public static final EndpointTestCase GET_PROJECTS_MAILING_LISTS_INVALID_FORMAT = TestCaseHelper
            .buildInvalidFormatCase(PROJECTS_MAILING_LISTS_URL, new String[] {}, ContentType.XML);

    /*
     * GET AVAILABLE PROJECTS
     */
    public static final EndpointTestCase GET_AVAILABLE_PROJECTS_MAILING_LISTS_SUCCESS = TestCaseHelper.buildSuccessCase(
            AVAILABLE_PROJECTS_MAILING_LISTS_URL, new String[] {}, SchemaNamespaceHelper.MAILING_LIST_MAPPING_SCHEMA_PATH);

    public static final EndpointTestCase GET_AVAILABLE_PROJECTS_MAILING_LISTS_INVALID_FORMAT = TestCaseHelper
            .buildInvalidFormatCase(AVAILABLE_PROJECTS_MAILING_LISTS_URL, new String[] {}, ContentType.XML);

    /*
     * PROJECTS MAILING LISTS BY NAME
     */
    public static final EndpointTestCase GET_PROJECTS_MAILING_LISTS_BY_ID_SUCCESS = TestCaseHelper.buildSuccessCase(
            PROJECT_MAILING_LISTS_URL, new String[] { "ee4j" }, SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_PROJECTS_MAILING_LISTS_BY_ID_INVALID_FORMAT = TestCaseHelper
            .buildInvalidFormatCase(PROJECT_MAILING_LISTS_URL, new String[] { "ee4j" }, ContentType.XML);

    /*
     * GET ALL
     */
    @Test
    void getMailingLists_success() {
        RestAssuredTemplates.testGet(GET_ALL_SUCCESS);
    }

    @Test
    void getMailingLists_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_ALL_SUCCESS);
    }

    @Test
    void getMailingLists_success_validSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_ALL_SUCCESS);
    }

    @Test
    void getMailingLists_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_ALL_INVALID_FORMAT);
    }

    /*
     * GET AVAILABLE
     */
    @Test
    void getAvailableMailingLists_success() {
        RestAssuredTemplates.testGet(GET_AVAILABLE_SUCCESS);
    }

    @Test
    void getAvailableMailingLists_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_AVAILABLE_SUCCESS);
    }

    @Test
    void getAvailableMailingLists_success_validSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_AVAILABLE_SUCCESS);
    }

    @Test
    void getAvailableMailingLists_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_AVAILABLE_INVALID_FORMAT);
    }

    /*
     * BY NAME
     */
    @Test
    void getMailingList_success() {
        RestAssuredTemplates.testGet(GET_MAILING_LISTS_BY_NAME_SUCCESS);
    }

    @Test
    void getMailingList_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_MAILING_LISTS_BY_NAME_SUCCESS);
    }

    @Test
    void getMailingList_success_validSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_MAILING_LISTS_BY_NAME_SUCCESS);
    }

    @Test
    void getMailingList_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_MAILING_LISTS_BY_NAME_INVALID_FORMAT);
    }

    @Test
    void getMailingList_failure_notFound() {
        RestAssuredTemplates.testGet(GET_MAILING_LISTS_BY_NAME_NOT_FOUND);
    }

    /*
     * GET PROJECTS MAILING LISTS
     */
    @Test
    void getProjectsMailingLists_success() {
        RestAssuredTemplates.testGet(GET_PROJECTS_MAILING_LISTS_SUCCESS);
    }

    @Test
    void getProjectsMailingLists_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_PROJECTS_MAILING_LISTS_SUCCESS);
    }

    @Test
    void getProjectsMailingLists_success_validSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_PROJECTS_MAILING_LISTS_SUCCESS);
    }

    @Test
    void getProjectsMailingLists_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_PROJECTS_MAILING_LISTS_INVALID_FORMAT);
    }

    /*
     * GET AVILABLE PROJECTS MAILING LISTS
     */
    @Test
    void getAvailableProjectsMailingLists_success() {
        RestAssuredTemplates.testGet(GET_AVAILABLE_PROJECTS_MAILING_LISTS_SUCCESS);
    }

    @Test
    void getAvailableProjectsMailingLists_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_AVAILABLE_PROJECTS_MAILING_LISTS_SUCCESS);
    }

    @Test
    void getAvailableProjectsMailingLists_success_validSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_AVAILABLE_PROJECTS_MAILING_LISTS_SUCCESS);
    }

    @Test
    void getAvailableProjectsMailingLists_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_AVAILABLE_PROJECTS_MAILING_LISTS_INVALID_FORMAT);
    }

    /*
     * GET PROJECT MAILING LIST BY ID
     */
    @Test
    void getProjectMailingList_success() {
        RestAssuredTemplates.testGet(GET_PROJECTS_MAILING_LISTS_BY_ID_SUCCESS);
    }

    @Test
    void getProjectMailingList_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_PROJECTS_MAILING_LISTS_BY_ID_SUCCESS);
    }

    @Test
    void getProjectMailingList_success_validSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_PROJECTS_MAILING_LISTS_BY_ID_SUCCESS);
    }

    @Test
    void getProjectMailingList_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_PROJECTS_MAILING_LISTS_BY_ID_INVALID_FORMAT);
    }
}
