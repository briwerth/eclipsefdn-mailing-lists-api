# eclipsefdn-mailing-lists-api

Eclipse Foundation API service for the retrieval of active mailing list data for projects within our ecosystem.

## Starting a development environment

Requirements:  

* Docker
* Java 11 >  
* mvn
* node.js + npm  
* make
* Running MariaDB instance

1. In MariaDB instance, create a new database called `eclipse` as that is what is configured out of the box. The table definitions for the database can be found under `./config/mariadb/ddl.sql`.
2. Run `make pre-setup`. This command will setup a basic .env file, using some basic variables used in Eclipse development to accelerate the generation of secret files.
3. Ensure that the `.env` file properties reflect the connection settings used for the MariaDB instance and the credentials needed to access the DB created in step 1.
4. Run `make setup` to finish the setup process for this API, generating the secret file under `./config/application/secret.properties`.
5. Run `compile-start` or `make compile-start-headless`. The first option binds the docker containers to the current terminal, and ends the processes when the terminal is closed. The second option starts the docker containers as part of the daemon and persists beyond the terminal session.

You can run your application in dev mode that enables live coding using:

```bash
make dev-start
```
